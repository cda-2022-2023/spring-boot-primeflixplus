package org.stcharles.primeflixplus.domain.service;

import org.stcharles.primeflixplus.domain.exception.CannotSubscribeToOfferException;
import org.stcharles.primeflixplus.domain.exception.NoSuchOfferException;
import org.stcharles.primeflixplus.domain.exception.NoSuchUserException;
import org.stcharles.primeflixplus.domain.exception.UserCannotBeUnderEighteenException;
import org.stcharles.primeflixplus.domain.model.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AccountingService {
    List<User> getAllUsers();
    Optional<User> getUser(UUID userId);

    User createUser(String lastName, String firstName, LocalDate birthDate, String email) throws UserCannotBeUnderEighteenException;
    void disableUser(UUID userId) throws NoSuchUserException;

    void addCreditCardForUser(UUID userId, String name, String number, LocalDate expiration) throws NoSuchUserException;
    void addPaypalAccountForUser(UUID userId, String account) throws NoSuchUserException;

    void enrollUser(UUID userId, UUID offerId) throws NoSuchUserException, NoSuchOfferException, CannotSubscribeToOfferException;
}
