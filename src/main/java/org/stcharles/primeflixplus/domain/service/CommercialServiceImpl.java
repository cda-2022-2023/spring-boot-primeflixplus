package org.stcharles.primeflixplus.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.stcharles.primeflixplus.data.repository.OfferRepository;
import org.stcharles.primeflixplus.domain.exception.NoSuchOfferException;
import org.stcharles.primeflixplus.domain.model.Offer;
import org.stcharles.primeflixplus.domain.model.Periodicity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CommercialServiceImpl implements CommercialService {
    public final OfferRepository offerRepository;

    @Autowired
    public CommercialServiceImpl(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    @Override
    public List<Offer> getAllOffers() {
        return offerRepository.findAll();
    }

    @Override
    public Optional<Offer> getOffer(UUID id) {
        return offerRepository.findById(id);
    }

    @Override
    @Transactional
    public Offer createOffer(String name, Periodicity periodicity, int pricing) {
        var offer = new Offer(name, periodicity, pricing);
        offerRepository.save(offer);
        return offer;
    }

    @Override
    @Transactional
    public void disableOffer(UUID id) throws NoSuchOfferException {
        offerRepository
                .findById(id)
                .orElseThrow(NoSuchOfferException::new)
                .disable();
    }
}
