package org.stcharles.primeflixplus.domain.service;

import org.stcharles.primeflixplus.domain.exception.NoSuchOfferException;
import org.stcharles.primeflixplus.domain.model.Offer;
import org.stcharles.primeflixplus.domain.model.Periodicity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommercialService {
    List<Offer> getAllOffers();
    Optional<Offer> getOffer(UUID id);

    Offer createOffer(String name, Periodicity periodicity, int pricing);
    void disableOffer(UUID id) throws NoSuchOfferException;
}
