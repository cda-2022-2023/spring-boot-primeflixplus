package org.stcharles.primeflixplus.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.stcharles.primeflixplus.data.repository.OfferRepository;
import org.stcharles.primeflixplus.data.repository.UserRepository;
import org.stcharles.primeflixplus.domain.exception.CannotSubscribeToOfferException;
import org.stcharles.primeflixplus.domain.exception.NoSuchOfferException;
import org.stcharles.primeflixplus.domain.exception.NoSuchUserException;
import org.stcharles.primeflixplus.domain.exception.UserCannotBeUnderEighteenException;
import org.stcharles.primeflixplus.domain.model.PaymentMethodCard;
import org.stcharles.primeflixplus.domain.model.PaymentMethodPaypal;
import org.stcharles.primeflixplus.domain.model.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AccountingServiceImpl implements AccountingService {
    private final UserRepository userRepository;
    private final OfferRepository offerRepository;

    @Autowired
    public AccountingServiceImpl(UserRepository userRepository, OfferRepository offerRepository) {
        this.userRepository = userRepository;
        this.offerRepository = offerRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> getUser(UUID userId) {
        return userRepository.findById(userId);
    }

    @Override
    @Transactional
    public User createUser(String lastName, String firstName, LocalDate birthDate, String email) throws UserCannotBeUnderEighteenException {
        var user = new User(lastName, firstName, birthDate, email);
        userRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    public void disableUser(UUID userId) throws NoSuchUserException {
        userRepository
                .findById(userId)
                .orElseThrow(NoSuchUserException::new)
                .disable();
    }

    @Override
    @Transactional
    public void addCreditCardForUser(UUID userId, String name, String number, LocalDate expiration) throws NoSuchUserException {
        var paymentMethodCard = new PaymentMethodCard(name, number, expiration);
        userRepository
                .findById(userId)
                .orElseThrow(NoSuchUserException::new)
                .addPaymentMethod(paymentMethodCard);
    }

    @Override
    @Transactional
    public void addPaypalAccountForUser(UUID userId, String account) throws NoSuchUserException {
        var paymentMethodPaypal = new PaymentMethodPaypal(account);
        userRepository
                .findById(userId)
                .orElseThrow(NoSuchUserException::new)
                .addPaymentMethod(paymentMethodPaypal);
    }

    @Override
    @Transactional
    public void enrollUser(UUID userId, UUID offerId) throws NoSuchUserException, NoSuchOfferException, CannotSubscribeToOfferException {
        var user = userRepository
                .findById(userId)
                .orElseThrow(NoSuchUserException::new);

        var offer = offerRepository
                .findById(offerId)
                .orElseThrow(NoSuchOfferException::new);

        user.subscribeTo(offer);
    }
}
