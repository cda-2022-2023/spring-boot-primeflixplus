package org.stcharles.primeflixplus.domain.model;

import jakarta.persistence.*;
import org.stcharles.primeflixplus.domain.exception.CannotSubscribeToOfferException;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Entity
@Table(name = "subscription")
public class Subscription {
    @Id
    private UUID id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "offer_id")
    private Offer offer;
    @Column(name = "start_date")
    private LocalDate startDate;
    @Column(name = "end_date")
    private LocalDate endDate;

    public Subscription(Offer offer) throws CannotSubscribeToOfferException {
        if (!offer.isAvailable()) {
            throw new CannotSubscribeToOfferException();
        }

        this.offer = offer;
        this.startDate = LocalDate.now();
    }

    protected Subscription() {
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Optional<LocalDate> getEndDate() {
        return Optional.ofNullable(endDate);
    }

    public boolean isActive() {
        return this.endDate == null;
    }

    public void end() {
        if (this.endDate == null) {
            this.endDate = LocalDate.now();
        }
    }
}
