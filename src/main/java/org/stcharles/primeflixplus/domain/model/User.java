package org.stcharles.primeflixplus.domain.model;

import jakarta.persistence.*;
import org.stcharles.primeflixplus.domain.exception.CannotSubscribeToOfferException;
import org.stcharles.primeflixplus.domain.exception.UserCannotBeUnderEighteenException;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Entity
@Table
public class User {
    @Id
    private UUID id;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "birthdate")
    private LocalDate birthdate;
    @Column(name = "email")
    private String email;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_id")
    private List<PaymentMethod> paymentMethods;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_id")
    private List<Subscription> subscriptions;
    @Column(name = "disabled")
    private boolean disabled;

    public User(String lastName, String firstName, LocalDate birthdate, String email) throws UserCannotBeUnderEighteenException {
        if (Period.between(birthdate, LocalDate.now()).getYears() < 18) {
            throw new UserCannotBeUnderEighteenException();
        }

        this.lastName = lastName;
        this.firstName = firstName;
        this.birthdate = birthdate;
        this.email = email;
        this.paymentMethods = new ArrayList<>();
        this.subscriptions = new ArrayList<>();
        this.disabled = false;
    }

    protected User() {}

    public UUID getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public String getEmail() {
        return email;
    }

    public void subscribeTo(Offer offer) throws CannotSubscribeToOfferException {
        endCurrentSubscription();
        var subscription = new Subscription(offer);
        this.subscriptions.add(subscription);
    }

    public boolean hasActiveSubscription() {
        return this.subscriptions
                .stream()
                .anyMatch(Subscription::isActive);
    }

    public void endCurrentSubscription() {
        this.subscriptions
                .stream()
                .filter(Subscription::isActive)
                .findAny()
                .ifPresent(Subscription::end);
    }

    public void disable() {
        endCurrentSubscription();
        this.paymentMethods.clear();
        this.disabled = true;
    }

    public void addPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethods.add(paymentMethod);
    }

    public List<PaymentMethod> getPaymentMethods() {
        return Collections.unmodifiableList(paymentMethods);
    }
}
