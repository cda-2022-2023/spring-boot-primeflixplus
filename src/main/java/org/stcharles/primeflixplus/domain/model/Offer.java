package org.stcharles.primeflixplus.domain.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import javax.swing.*;
import java.util.UUID;

@Entity
@Table(name = "offer")
public class Offer {
    @Id
    private UUID id;
    @Column(name = "name")
    private String name;
    @Column(name = "available")
    private boolean available;
    @Column(name = "periodicity")
    private Periodicity periodicity;
    @Column(name = "pricing")
    private int pricing;

    public Offer(String name, Periodicity periodicity, int pricing) {
        this.name = name;
        this.available = true;
        this.periodicity = periodicity;
        this.pricing = pricing;
    }

    protected Offer() {
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isAvailable() {
        return available;
    }

    public Periodicity getPeriodicity() {
        return periodicity;
    }

    public int getPricing() {
        return pricing;
    }

    public void disable() {
        this.available = false;
    }
}