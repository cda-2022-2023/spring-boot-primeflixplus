package org.stcharles.primeflixplus.domain.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "payment_method_paypal")
public class PaymentMethodPaypal extends PaymentMethod {
    @Column(name = "account")
    private String account;

    public PaymentMethodPaypal(String account) {
        this.account = account;
    }

    protected PaymentMethodPaypal() {
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public String getAccount() {
        return account;
    }
}
