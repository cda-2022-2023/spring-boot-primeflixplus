package org.stcharles.primeflixplus.domain.model;

import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Table(name = "payment_method")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class PaymentMethod {
    @Id
    private UUID id;

    public UUID getId() {
        return id;
    }

    public abstract boolean isValid();
}
