package org.stcharles.primeflixplus.domain.model;

public enum Periodicity {
    MONTHLY,
    QUARTERLY,
    YEARLY,
}
