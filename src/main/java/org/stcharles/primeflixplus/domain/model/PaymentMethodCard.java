package org.stcharles.primeflixplus.domain.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.time.LocalDate;

@Entity
@Table(name = "payment_method_card")
public class PaymentMethodCard extends PaymentMethod {
    @Column(name = "name")
    private String name;
    @Column(name = "number")
    private String number;
    @Column(name = "expiration")
    private LocalDate expiration;

    public PaymentMethodCard(String name, String number, LocalDate expiration) {
        super();
        this.name = name;
        this.number = number;
        this.expiration = expiration;
    }

    protected PaymentMethodCard() {
    }

    @Override
    public boolean isValid() {
        return LocalDate.now().isBefore(expiration);
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public LocalDate getExpiration() {
        return expiration;
    }
}
