package org.stcharles.primeflixplus.controller.user.dto;

public class PaymentMethodPaypalJson extends PaymentMethodJson {
    private final String account;

    public PaymentMethodPaypalJson(String account) {
        this.account = account;
    }

    public String getAccount() {
        return account;
    }
}
