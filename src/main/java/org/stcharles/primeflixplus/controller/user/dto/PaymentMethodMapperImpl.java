package org.stcharles.primeflixplus.controller.user.dto;

import org.springframework.stereotype.Component;
import org.stcharles.primeflixplus.domain.model.PaymentMethod;
import org.stcharles.primeflixplus.domain.model.PaymentMethodCard;
import org.stcharles.primeflixplus.domain.model.PaymentMethodPaypal;

@Component
public class PaymentMethodMapperImpl implements PaymentMethodMapper {
    @Override
    public PaymentMethodJson map(PaymentMethod entity) {
        if (entity instanceof PaymentMethodCard) {
            return map((PaymentMethodCard) entity);
        } else {
            return map((PaymentMethodPaypal) entity);
        }
    }

    private PaymentMethodCardJson map(PaymentMethodCard paymentMethodCard) {
        return new PaymentMethodCardJson(
                paymentMethodCard.getName(),
                paymentMethodCard.getNumber(),
                paymentMethodCard.getExpiration()
        );
    }

    private PaymentMethodPaypalJson map(PaymentMethodPaypal paymentMethodPaypal) {
        return new PaymentMethodPaypalJson(
                paymentMethodPaypal.getAccount()
        );
    }
}
