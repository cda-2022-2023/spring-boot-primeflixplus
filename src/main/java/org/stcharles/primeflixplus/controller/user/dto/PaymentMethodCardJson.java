package org.stcharles.primeflixplus.controller.user.dto;

import java.time.LocalDate;

public class PaymentMethodCardJson extends PaymentMethodJson {
    private final String name;
    private final String number;
    private final LocalDate expiration;

    public PaymentMethodCardJson(String name, String number, LocalDate expiration) {
        this.name = name;
        this.number = number;
        this.expiration = expiration;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public LocalDate getExpiration() {
        return expiration;
    }
}
