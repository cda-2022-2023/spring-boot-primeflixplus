package org.stcharles.primeflixplus.controller.user.dto;

import java.time.LocalDate;
import java.util.UUID;

public record UserJson(
        UUID id,
        String firstName,
        String lastName,
        LocalDate birthDate,
        String email
) {}
