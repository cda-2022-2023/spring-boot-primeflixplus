package org.stcharles.primeflixplus.controller.user.dto;

import org.springframework.stereotype.Component;
import org.stcharles.primeflixplus.domain.model.User;

@Component
public class UserMapperImpl implements UserMapper {
    @Override
    public UserJson map(User entity) {
        return new UserJson(
                entity.getId(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getBirthdate(),
                entity.getEmail()
        );
    }
}
