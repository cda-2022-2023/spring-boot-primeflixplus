package org.stcharles.primeflixplus.controller.user.dto;

import org.stcharles.primeflixplus.controller.Mapper;
import org.stcharles.primeflixplus.domain.model.PaymentMethod;

public interface PaymentMethodMapper extends Mapper<PaymentMethod, PaymentMethodJson> {
}
