package org.stcharles.primeflixplus.controller.user.dto;

import org.stcharles.primeflixplus.controller.Mapper;
import org.stcharles.primeflixplus.domain.model.User;

public interface UserMapper extends Mapper<User, UserJson> {
}
