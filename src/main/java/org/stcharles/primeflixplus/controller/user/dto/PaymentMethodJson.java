package org.stcharles.primeflixplus.controller.user.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = PaymentMethodCardJson.class, name = "card"),
        @JsonSubTypes.Type(value = PaymentMethodPaypalJson.class, name = "paypal")
})
public abstract class PaymentMethodJson {
}
