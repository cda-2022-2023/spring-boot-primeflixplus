package org.stcharles.primeflixplus.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.stcharles.primeflixplus.controller.InvalidJsonInputException;
import org.stcharles.primeflixplus.controller.NotFoundException;
import org.stcharles.primeflixplus.controller.user.dto.*;
import org.stcharles.primeflixplus.domain.exception.NoSuchUserException;
import org.stcharles.primeflixplus.domain.exception.UserCannotBeUnderEighteenException;
import org.stcharles.primeflixplus.domain.model.User;
import org.stcharles.primeflixplus.domain.service.AccountingService;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/users")
public class UserController {
    private final AccountingService accountingService;
    private final UserMapper userMapper;
    private final PaymentMethodMapper paymentMethodMapper;

    @Autowired
    public UserController(AccountingService accountingService, UserMapper userMapper, PaymentMethodMapper paymentMethodMapper) {
        this.accountingService = accountingService;
        this.userMapper = userMapper;
        this.paymentMethodMapper = paymentMethodMapper;
    }

    @GetMapping
    public List<UserJson> getAllUsers() {
        return accountingService
                .getAllUsers()
                .stream()
                .map(userMapper::map)
                .toList();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public UUID createUser(@RequestBody UserJson userJson) {
        try {
            var user = accountingService
                    .createUser(
                            userJson.lastName(),
                            userJson.firstName(),
                            userJson.birthDate(),
                            userJson.email()
                    );
            return user.getId();

        } catch (UserCannotBeUnderEighteenException e) {
            throw new InvalidJsonInputException();
        }
    }

    @GetMapping("/{id}")
    public UserJson getUser(@PathVariable UUID id) {
        return accountingService
                .getUser(id)
                .map(userMapper::map)
                .orElseThrow(NotFoundException::new);
    }

    @GetMapping("/{userId}/paymentMethods")
    public List<PaymentMethodJson> getUserPaymentMethods(@PathVariable UUID userId) {
        var paymentMethods = accountingService
                .getUser(userId)
                .map(User::getPaymentMethods)
                .orElseThrow(NotFoundException::new);

        return paymentMethods
                .stream()
                .map(paymentMethodMapper::map)
                .toList();
    }

    @PostMapping("/{userId}/paymentMethods")
    public void createUserPaymentMethod(@PathVariable UUID userId, @RequestBody PaymentMethodJson paymentMethodJson) {
        try {
            if (paymentMethodJson instanceof PaymentMethodCardJson paymentMethodCardJson) {
                accountingService
                        .addCreditCardForUser(
                                userId,
                                paymentMethodCardJson.getName(),
                                paymentMethodCardJson.getNumber(),
                                paymentMethodCardJson.getExpiration()
                        );

            } else if (paymentMethodJson instanceof PaymentMethodPaypalJson paymentMethodPaypalJson) {
                accountingService
                        .addPaypalAccountForUser(
                                userId,
                                paymentMethodPaypalJson.getAccount()
                        );
            }

        } catch (NoSuchUserException e) {
            throw new NotFoundException();
        }
    }
}
