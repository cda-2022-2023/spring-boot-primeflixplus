package org.stcharles.primeflixplus.controller;

public interface Mapper<T, U> {
    U map (T entity);
}
