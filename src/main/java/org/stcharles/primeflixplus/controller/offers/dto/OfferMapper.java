package org.stcharles.primeflixplus.controller.offers.dto;

import org.stcharles.primeflixplus.controller.Mapper;
import org.stcharles.primeflixplus.domain.model.Offer;

public interface OfferMapper extends Mapper<Offer, OfferJson> {
}
