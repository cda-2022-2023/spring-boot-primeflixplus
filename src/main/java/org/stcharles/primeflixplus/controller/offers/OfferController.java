package org.stcharles.primeflixplus.controller.offers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.stcharles.primeflixplus.controller.InvalidJsonInputException;
import org.stcharles.primeflixplus.controller.NotFoundException;
import org.stcharles.primeflixplus.controller.offers.dto.OfferJson;
import org.stcharles.primeflixplus.controller.offers.dto.OfferMapper;
import org.stcharles.primeflixplus.domain.exception.NoSuchOfferException;
import org.stcharles.primeflixplus.domain.model.Offer;
import org.stcharles.primeflixplus.domain.model.Periodicity;
import org.stcharles.primeflixplus.domain.service.CommercialService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/offers")
public class OfferController {
    private final CommercialService commercialService;
    private final OfferMapper offerMapper;

    @Autowired
    public OfferController(CommercialService commercialService, OfferMapper offerMapper) {
        this.commercialService = commercialService;
        this.offerMapper = offerMapper;
    }

    @GetMapping
    public List<OfferJson> getAllOffers() {
        return commercialService
                .getAllOffers()
                .stream()
                .map(offerMapper::map)
                .toList();
    }

    @GetMapping("/{id}")
    public OfferJson getOffer(@PathVariable UUID id) {
        return commercialService
                .getOffer(id)
                .map(offerMapper::map)
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    public UUID createOffer(OfferJson offerJson) {
        var periodicity = switch (offerJson.periodicity()) {
            case "monthly" -> Periodicity.MONTHLY;
            case "quarterly" -> Periodicity.QUARTERLY;
            case "yearly" -> Periodicity.YEARLY;

            default -> throw new InvalidJsonInputException();
        };

        var offer = commercialService
                .createOffer(
                        offerJson.name(),
                        periodicity,
                        offerJson.pricing()
                );

        return offer.getId();
    }

    @PutMapping("/{id}")
    public void updateOffer(@PathVariable UUID id, OfferJson offerJson) {
        try {
            if (!offerJson.available()) {
                commercialService.disableOffer(id);
            }
        } catch (NoSuchOfferException e) {
            throw new NotFoundException();
        }
    }
}
