package org.stcharles.primeflixplus.controller.offers.dto;

import org.springframework.stereotype.Component;
import org.stcharles.primeflixplus.domain.model.Offer;

@Component
public class OfferMapperImpl implements OfferMapper {
    @Override
    public OfferJson map(Offer entity) {
        var periodicity = switch (entity.getPeriodicity()) {
            case MONTHLY -> "monthly";
            case QUARTERLY -> "quarterly";
            case YEARLY -> "yearly";
        };

        var multiline = """
                SELECT *
                FROM toto
                """.stripIndent()
                .toLowerCase();

        return new OfferJson(
                entity.getId(),
                entity.getName(),
                entity.isAvailable(),
                periodicity,
                entity.getPricing()
        );
    }
}
