package org.stcharles.primeflixplus.controller.offers.dto;

import java.util.UUID;

public record OfferJson(
        UUID id,
        String name,
        boolean available,
        String periodicity,
        int pricing
){}
