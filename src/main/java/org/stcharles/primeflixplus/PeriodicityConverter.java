package org.stcharles.primeflixplus;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.stcharles.primeflixplus.domain.model.Periodicity;

@Converter(autoApply = true)
public class PeriodicityConverter implements AttributeConverter<Periodicity, String> {
    @Override
    public String convertToDatabaseColumn(Periodicity periodicity) {
        return switch (periodicity) {
            case MONTHLY -> "monthly";
            case QUARTERLY -> "quarterly";
            case YEARLY -> "yearly";
        };
    }

    @Override
    public Periodicity convertToEntityAttribute(String s) {
        return switch (s) {
            case "monthly" -> Periodicity.MONTHLY;
            case "quarterly" -> Periodicity.QUARTERLY;
            case "yearly" -> Periodicity.YEARLY;

            default -> throw new RuntimeException("Mapping error");
        };
    }
}
