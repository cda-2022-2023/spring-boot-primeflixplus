package org.stcharles.primeflixplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimeflixplusApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimeflixplusApplication.class, args);
	}

}
