INSERT INTO "offer" ("id", "name", "available", "periodicity", "pricing")
VALUES (uuid_generate_v4(), 'Basic', true, 'monthly', 9.90),
       (uuid_generate_v4(), 'Family', true, 'monthly', 12.90);

INSERT INTO "user" ("id", "last_name", "first_name", "birthdate", "email", "disabled")
VALUES (uuid_generate_v4(), 'Naudin', 'Thomas', '1987-02-13', 'thomas.naudin@stcharles-stecroix.org', false);