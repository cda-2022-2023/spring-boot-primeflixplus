CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "user"
(
    "id" UUID NOT NULL,
    "last_name" VARCHAR(255) NOT NULL,
    "first_name" VARCHAR(255) NOT NULL,
    "birthdate" DATE NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "disabled" BOOLEAN NOT NULL,

    PRIMARY KEY ("id")
);

CREATE TABLE "payment_method"
(
    "id" UUID NOT NULL,
    "user_id" UUID NOT NULL,

    PRIMARY KEY ("id"),
    FOREIGN KEY ("user_id") REFERENCES "user"("id")
);

CREATE TABLE "payment_method_card"
(
    "id" UUID NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "number" VARCHAR(255) NOT NULL,
    "expiration" DATE NOT NULL,

    PRIMARY KEY ("id"),
    FOREIGN KEY ("id") REFERENCES "payment_method"("id")
);

CREATE TABLE "payment_method_paypal"
(
    "id" UUID NOT NULL,
    "account" VARCHAR(255) NOT NULL,

    PRIMARY KEY ("id"),
    FOREIGN KEY ("id") REFERENCES "payment_method"("id")
);

CREATE TYPE "periodicity" AS ENUM ('monthly', 'quarterly', 'yearly');

CREATE TABLE "offer"
(
    "id" UUID NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "available" BOOLEAN NOT NULL,
    "periodicity" periodicity NOT NULL,
    "pricing" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

CREATE TABLE "subscription"
(
    "id" UUID NOT NULL,
    "user_id" UUID NOT NULL,
    "offer_id" UUID NOT NULL,
    "start_date" DATE NOT NULL,
    "end_date" DATE,

    PRIMARY KEY ("id"),
    FOREIGN KEY ("user_id") REFERENCES "user"("id"),
    FOREIGN KEY ("offer_id") REFERENCES "offer"("id")
);